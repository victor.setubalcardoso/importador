# Importador - Agibank

O Importador é um sistema de analise de dados. Dado um diretório, 
o sistema importa os arquivos de um determinado formato, 
processa as informações e gera em um outro diretório um arquivo com base na análise feita.
Ele está configurado como uma rotina a ser executada a cada minuto.
Esse agendamento pode ser alterado caso seja necessário utilizando uma expressão _cron_ no arquivo JobImportador.java.


# Pré-requisitos

1) Baixar e instalar o **Maven**.

2) Baixar e instalar o **JDK**.

3) Criar os seguintes diretórios na pasta pessoal do usuário: 
    - **/data/in**: arquivos de entradas a serem importados;
    - **/data/processados**: arquivos de entradas que já foram importados e processados;
    - **/data/out**: arquivos com relatório de saída dos processamentos;
    
# Passo-a-passo para execução da aplicação

1) Executar o seguinte comando dentro do diretório raiz da aplicação, 
para geração do arquivo .jar:
 
 - `mvn package`
 
2) Executar o seguinte comando para execução do arquivo _.jar_ gerado no diretório **target** (diretório gerado na raiz da aplicação após o comando utilizado anteriormente):

- `java -jar target/importador-0.0.1-SNAPSHOT.jar`

# Considerações finais

Após a execução dos comandos acima citados, a importação dos arquivos será executada a cada minuto.




