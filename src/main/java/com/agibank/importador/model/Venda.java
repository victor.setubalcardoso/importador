package com.agibank.importador.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Venda {

    private String id;
    private List<Item> items = new ArrayList<>();
    private BigDecimal valorTotal;
    private String vendedor;
}
