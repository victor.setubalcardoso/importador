package com.agibank.importador;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;


@EnableScheduling
@SpringBootApplication
public class ImportadorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImportadorApplication.class, args);
	}



}


