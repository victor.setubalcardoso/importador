package com.agibank.importador.util;

public interface Mensagens {


    String ERRO_LEITURA = "Erro ao ler arquivos entrada para iniciar processamento";
    String ERRO_GRAVACAO = "Erro ao gerar arquivo de saída.";
}
