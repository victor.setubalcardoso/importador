package com.agibank.importador.util;

import java.io.File;

public interface Constants {


    String PATH_FILES_IN = System.getProperty("user.home") + File.separator + "data" + File.separator + "in" + File.separator;
    String PATH_FILE_OUT = System.getProperty("user.home") + File.separator + "data" + File.separator + "out" + File.separator;
    String PATH_FILE_PROCESSADOS = System.getProperty("user.home") + File.separator + "data" + File.separator + "processados" + File.separator;
    String NAME_FILE_OUT = "file.done.dat";
    String ID_VENDEDOR = "001";
    String ID_CLIENTE = "002";
    String ID_VENDA = "003";
    String CARACTER_SPLIT_LAYOUT = "ç";
    String QUANTIDADE_CLIENTES = "QUANTIDADE_CLIENTES: ";
    String QUANTIDADE_VENDEDOR = "QUANTIDADE_VENDEDOR: ";
    String VENDA_MAIS_CARA = "VENDA_MAIS_CARA: ";
    String PIOR_VENDEDOR = "PIOR_VENDEDOR: ";
    String FORMATO_DATA_HORA_NOME_ARQUIVO = "ddMMyyyyHHmmss";
    String FORMATO_DATA_HORA = "HH:mm:ss dd/MM/yyyy";
    String INICIANDO_IMPORTACAO = "Iniciando importação em: ";
    String TEMPO_PROCESSAMENTO = "Tempo de processamento (em segundos): ";
    String FINALIZANDO_IMPORTACAO = "Finalizando importação em: ";
}
