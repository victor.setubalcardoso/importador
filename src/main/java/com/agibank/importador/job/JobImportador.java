package com.agibank.importador.job;

import com.agibank.importador.model.Item;
import com.agibank.importador.model.Venda;
import com.agibank.importador.util.Constants;
import com.agibank.importador.util.Mensagens;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class JobImportador {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());


    @Scheduled(cron = "0 0/1 * * * ?")
    public void importarArquivos() {

        long tempoInicial = System.currentTimeMillis();
        logger.info(Constants.INICIANDO_IMPORTACAO  + new SimpleDateFormat(Constants.FORMATO_DATA_HORA).format(new Date()));

        File[] arquivos = obterArquivos();
        List<String> linhas = new ArrayList<>();
        for (File file : arquivos) {
            if (file.isFile()) {
                linhas.addAll(obterLinhas(file.getName()));
            }
        }

        long quantidadeClientes = linhas.stream().filter(dado -> dado.startsWith(Constants.ID_CLIENTE)).count();
        long quantidadeVendedor = linhas.stream().filter(dado -> dado.startsWith(Constants.ID_VENDEDOR)).count();

        new Thread(() -> {
            List<Venda> vendas = obterVendasOrdenadasPorPreco(linhas);
            Venda vendaMaisCara = vendas.size() > 0 ? vendas.get(0) : null;
            Map.Entry<String, BigDecimal> piorVendedor = obterPiorVendedor(vendas);
            String[] resultado = obterResultado(quantidadeClientes, quantidadeVendedor, vendaMaisCara, piorVendedor);
            if (existeArquivosParaProcessar(arquivos)) {
                gerarArquivo(Arrays.asList(resultado));
            }
        }).start();

        moveArquivosProcessados(arquivos);

        long tempoFinal = System.currentTimeMillis();
        logger.info(Constants.TEMPO_PROCESSAMENTO + (tempoFinal - tempoInicial) / 1000d);
        logger.info(Constants.FINALIZANDO_IMPORTACAO + new SimpleDateFormat(Constants.FORMATO_DATA_HORA).format(new Date()));
    }


    private void moveArquivosProcessados(File[] arquivos) {
        for (File file : arquivos) {
            if (file.isFile() && file.renameTo(new File(Constants.PATH_FILE_PROCESSADOS.concat(file.getName())))) {
                file.delete();
            }
        }
    }

    private void gerarArquivo(List<String> resultado) {
        try {
            String nomeArquivo = Constants.PATH_FILE_OUT
                    .concat(new SimpleDateFormat(Constants.FORMATO_DATA_HORA_NOME_ARQUIVO)
                            .format(new Date())
                            .concat("_"
                                    .concat(Constants.NAME_FILE_OUT)));
            FileWriter writer = new FileWriter(nomeArquivo);
            for(String dado: resultado) {
                writer.write(dado + System.lineSeparator());
            }
            writer.close();
        } catch (IOException e) {
            logger.error(Mensagens.ERRO_GRAVACAO);
            e.printStackTrace();
        }

    }

    private File[] obterArquivos() {
        File folder = new File(Constants.PATH_FILES_IN);
        return folder.listFiles();
    }

    private List<String> obterLinhas(String arquivo) {
        try {
            return Files.readAllLines(new File(Constants.PATH_FILES_IN.concat(arquivo)).toPath());
        } catch (IOException e) {
            logger.error(Mensagens.ERRO_LEITURA);
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    private List<Venda> obterVendasOrdenadasPorPreco(List<String> linhas) {
        return linhas.stream()
                .filter(linha -> linha.startsWith(Constants.ID_VENDA))
                .map(linha -> linha.split(Constants.CARACTER_SPLIT_LAYOUT))
                .map(dados -> {
                    List<Item> items = obterItemsVenda(dados[2]);
                    Venda venda = new Venda();
                    venda.setId(dados[1]);
                    venda.setItems(items);
                    venda.setValorTotal(items.stream()
                            .map(x -> BigDecimal.valueOf(x.getQuantidade()).multiply(x.getPreco()))
                            .reduce(BigDecimal.ZERO, BigDecimal::add));
                    venda.setVendedor(dados[3]);
                    return venda;
                }).sorted(Comparator.comparing(Venda::getValorTotal)).collect(Collectors.toList());
    }


    private List<Item> obterItemsVenda(String items) {
        return Arrays.stream(items.replace("[","").replace("]","").split(","))
                .map(i -> {
                    String[] item = i.split("-");
                    return new Item(Integer.parseInt(item[0]), Integer.parseInt(item[1]), new BigDecimal(item[2]));
                }).collect(Collectors.toList());
    }

    private Map.Entry<String, BigDecimal> obterPiorVendedor(List<Venda> vendas) {
        Iterator<Map.Entry<String, BigDecimal>> iterator = vendas.stream()
                .collect(Collectors.groupingBy(Venda::getVendedor, Collectors.reducing(BigDecimal.ZERO, Venda::getValorTotal, BigDecimal::add)))
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue, LinkedHashMap::new))
                .entrySet().iterator();
        return iterator.hasNext() ? iterator.next() : null;
    }

    private boolean existeArquivosParaProcessar(File[] arquivos) {
        return arquivos.length > 0;
    }

    private String[] obterResultado(long quantidadeClientes, long quantidadeVendedor, Venda vendaMaisCara, Map.Entry<String, BigDecimal> piorVendedor) {
        return new String[]{Constants.QUANTIDADE_CLIENTES.concat(String.valueOf(quantidadeClientes)),
                Constants.QUANTIDADE_VENDEDOR.concat(String.valueOf(quantidadeVendedor)),
                Constants.VENDA_MAIS_CARA.concat((vendaMaisCara != null ? vendaMaisCara.getId() : "")),
                Constants.PIOR_VENDEDOR.concat(piorVendedor != null ? String.valueOf(piorVendedor.getKey()) : "")
        };
    }

}
